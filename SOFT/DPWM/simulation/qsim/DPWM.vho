-- Copyright (C) 2023  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 22.1std.1 Build 917 02/14/2023 SC Lite Edition"

-- DATE "12/23/2023 18:13:46"

-- 
-- Device: Altera EPM570M100I5 Package MBGA100
-- 

-- 
-- This VHDL file should be used for Questa Intel FPGA (VHDL) only
-- 

LIBRARY IEEE;
LIBRARY MAXII;
USE IEEE.STD_LOGIC_1164.ALL;
USE MAXII.MAXII_COMPONENTS.ALL;

ENTITY 	DPWM IS
    PORT (
	CLK : IN std_logic;
	DATA : IN std_logic_vector(7 DOWNTO 0);
	PWM : OUT std_logic
	);
END DPWM;

-- Design Ports Information


ARCHITECTURE structure OF DPWM IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_DATA : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_PWM : std_logic;
SIGNAL \LessThan0~5\ : std_logic;
SIGNAL \LessThan0~10\ : std_logic;
SIGNAL \LessThan0~15\ : std_logic;
SIGNAL \LessThan0~20\ : std_logic;
SIGNAL \LessThan0~25\ : std_logic;
SIGNAL \LessThan0~30\ : std_logic;
SIGNAL \LessThan0~35\ : std_logic;
SIGNAL \Equal0~1_combout\ : std_logic;
SIGNAL \Equal0~0_combout\ : std_logic;
SIGNAL \CLK~combout\ : std_logic;
SIGNAL \process_0~0_combout\ : std_logic;
SIGNAL \process_0~1_combout\ : std_logic;
SIGNAL \process_0~2_combout\ : std_logic;
SIGNAL \process_0~5_combout\ : std_logic;
SIGNAL \process_0~6_combout\ : std_logic;
SIGNAL \process_0~7_combout\ : std_logic;
SIGNAL \process_0~8_combout\ : std_logic;
SIGNAL \COUNT[0]~15\ : std_logic;
SIGNAL \COUNT[0]~15COUT1_17\ : std_logic;
SIGNAL \COUNT[1]~13\ : std_logic;
SIGNAL \COUNT[1]~13COUT1_18\ : std_logic;
SIGNAL \COUNT[2]~11\ : std_logic;
SIGNAL \COUNT[2]~11COUT1_19\ : std_logic;
SIGNAL \COUNT[3]~9\ : std_logic;
SIGNAL \COUNT[3]~9COUT1_20\ : std_logic;
SIGNAL \COUNT[4]~7\ : std_logic;
SIGNAL \process_0~3_combout\ : std_logic;
SIGNAL \process_0~4_combout\ : std_logic;
SIGNAL \COUNT[5]~5\ : std_logic;
SIGNAL \COUNT[5]~5COUT1_21\ : std_logic;
SIGNAL \COUNT[6]~3\ : std_logic;
SIGNAL \COUNT[6]~3COUT1_22\ : std_logic;
SIGNAL \LessThan0~37_cout0\ : std_logic;
SIGNAL \LessThan0~37COUT1_41\ : std_logic;
SIGNAL \LessThan0~32_cout0\ : std_logic;
SIGNAL \LessThan0~32COUT1_42\ : std_logic;
SIGNAL \LessThan0~27_cout0\ : std_logic;
SIGNAL \LessThan0~27COUT1_43\ : std_logic;
SIGNAL \LessThan0~22_cout0\ : std_logic;
SIGNAL \LessThan0~22COUT1_44\ : std_logic;
SIGNAL \LessThan0~17_cout\ : std_logic;
SIGNAL \LessThan0~12_cout0\ : std_logic;
SIGNAL \LessThan0~12COUT1_45\ : std_logic;
SIGNAL \LessThan0~7_cout0\ : std_logic;
SIGNAL \LessThan0~7COUT1_46\ : std_logic;
SIGNAL \LessThan0~0_combout\ : std_logic;
SIGNAL \PWM~0_combout\ : std_logic;
SIGNAL INCREM : std_logic_vector(7 DOWNTO 0);
SIGNAL \DATA~combout\ : std_logic_vector(7 DOWNTO 0);
SIGNAL COUNT : std_logic_vector(7 DOWNTO 0);

BEGIN

ww_CLK <= CLK;
ww_DATA <= DATA;
PWM <= ww_PWM;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: PIN_J6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[4]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(4),
	combout => \DATA~combout\(4));

-- Location: PIN_K7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[6]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(6),
	combout => \DATA~combout\(6));

-- Location: PIN_L7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[5]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(5),
	combout => \DATA~combout\(5));

-- Location: PIN_E11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[7]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(7),
	combout => \DATA~combout\(7));

-- Location: LC_X10_Y4_N9
\Equal0~1\ : maxii_lcell
-- Equation(s):
-- \Equal0~1_combout\ = (\DATA~combout\(4) & (\DATA~combout\(6) & (\DATA~combout\(5) & \DATA~combout\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(4),
	datab => \DATA~combout\(6),
	datac => \DATA~combout\(5),
	datad => \DATA~combout\(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal0~1_combout\);

-- Location: PIN_F9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[0]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(0),
	combout => \DATA~combout\(0));

-- Location: PIN_K6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[2]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(2),
	combout => \DATA~combout\(2));

-- Location: PIN_K8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[3]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(3),
	combout => \DATA~combout\(3));

-- Location: PIN_L8,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\DATA[1]~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_DATA(1),
	combout => \DATA~combout\(1));

-- Location: LC_X8_Y4_N5
\Equal0~0\ : maxii_lcell
-- Equation(s):
-- \Equal0~0_combout\ = (\DATA~combout\(0) & (\DATA~combout\(2) & (\DATA~combout\(3) & \DATA~combout\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(0),
	datab => \DATA~combout\(2),
	datac => \DATA~combout\(3),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal0~0_combout\);

-- Location: PIN_F2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\CLK~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input")
-- pragma translate_on
PORT MAP (
	oe => GND,
	padio => ww_CLK,
	combout => \CLK~combout\);

-- Location: LC_X10_Y3_N3
\process_0~0\ : maxii_lcell
-- Equation(s):
-- \process_0~0_combout\ = (!\DATA~combout\(2) & (!\DATA~combout\(1) & (!\DATA~combout\(3) & !\DATA~combout\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(2),
	datab => \DATA~combout\(1),
	datac => \DATA~combout\(3),
	datad => \DATA~combout\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~0_combout\);

-- Location: LC_X10_Y3_N2
\process_0~1\ : maxii_lcell
-- Equation(s):
-- \process_0~1_combout\ = (\DATA~combout\(4)) # ((\DATA~combout\(6)) # ((\DATA~combout\(5)) # (!\process_0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "feff",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(4),
	datab => \DATA~combout\(6),
	datac => \DATA~combout\(5),
	datad => \process_0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~1_combout\);

-- Location: LC_X10_Y3_N7
\process_0~2\ : maxii_lcell
-- Equation(s):
-- \process_0~2_combout\ = (((\DATA~combout\(7)) # (\process_0~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "fff0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datac => \DATA~combout\(7),
	datad => \process_0~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~2_combout\);

-- Location: LC_X10_Y3_N8
\INCREM[7]\ : maxii_lcell
-- Equation(s):
-- INCREM(7) = ((GLOBAL(\process_0~2_combout\) & (!\process_0~1_combout\)) # (!GLOBAL(\process_0~2_combout\) & ((INCREM(7)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5f50",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~1_combout\,
	datac => \process_0~2_combout\,
	datad => INCREM(7),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => INCREM(7));

-- Location: LC_X10_Y3_N0
\process_0~5\ : maxii_lcell
-- Equation(s):
-- \process_0~5_combout\ = (\process_0~0_combout\ & (((\DATA~combout\(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "a0a0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~0_combout\,
	datac => \DATA~combout\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~5_combout\);

-- Location: LC_X10_Y3_N4
\INCREM[4]\ : maxii_lcell
-- Equation(s):
-- INCREM(4) = ((GLOBAL(\process_0~2_combout\) & (\process_0~5_combout\)) # (!GLOBAL(\process_0~2_combout\) & ((INCREM(4)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "afa0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~5_combout\,
	datac => \process_0~2_combout\,
	datad => INCREM(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => INCREM(4));

-- Location: LC_X8_Y4_N6
\process_0~6\ : maxii_lcell
-- Equation(s):
-- \process_0~6_combout\ = (!\DATA~combout\(0) & (!\DATA~combout\(2) & (\DATA~combout\(3) & !\DATA~combout\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(0),
	datab => \DATA~combout\(2),
	datac => \DATA~combout\(3),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~6_combout\);

-- Location: LC_X8_Y4_N7
\INCREM[3]\ : maxii_lcell
-- Equation(s):
-- INCREM(3) = (GLOBAL(\process_0~2_combout\) & (\process_0~6_combout\)) # (!GLOBAL(\process_0~2_combout\) & (((INCREM(3)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "acac",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~6_combout\,
	datab => INCREM(3),
	datac => \process_0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => INCREM(3));

-- Location: LC_X8_Y4_N8
\process_0~7\ : maxii_lcell
-- Equation(s):
-- \process_0~7_combout\ = (!\DATA~combout\(0) & (\DATA~combout\(2) & ((!\DATA~combout\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0044",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(0),
	datab => \DATA~combout\(2),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~7_combout\);

-- Location: LC_X8_Y4_N0
\INCREM[2]\ : maxii_lcell
-- Equation(s):
-- INCREM(2) = (GLOBAL(\process_0~2_combout\) & (\process_0~7_combout\)) # (!GLOBAL(\process_0~2_combout\) & (((INCREM(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "acac",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~7_combout\,
	datab => INCREM(2),
	datac => \process_0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => INCREM(2));

-- Location: LC_X8_Y4_N2
\process_0~8\ : maxii_lcell
-- Equation(s):
-- \process_0~8_combout\ = (!\DATA~combout\(0) & (((\DATA~combout\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5500",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(0),
	datad => \DATA~combout\(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~8_combout\);

-- Location: LC_X9_Y4_N9
\INCREM[1]\ : maxii_lcell
-- Equation(s):
-- INCREM(1) = ((GLOBAL(\process_0~2_combout\) & (\process_0~8_combout\)) # (!GLOBAL(\process_0~2_combout\) & ((INCREM(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "afa0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~8_combout\,
	datac => \process_0~2_combout\,
	datad => INCREM(1),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => INCREM(1));

-- Location: LC_X9_Y4_N8
\INCREM[0]\ : maxii_lcell
-- Equation(s):
-- INCREM(0) = ((GLOBAL(\process_0~2_combout\) & (\DATA~combout\(0))) # (!GLOBAL(\process_0~2_combout\) & ((INCREM(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "afa0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(0),
	datac => \process_0~2_combout\,
	datad => INCREM(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => INCREM(0));

-- Location: LC_X9_Y4_N0
\COUNT[0]\ : maxii_lcell
-- Equation(s):
-- COUNT(0) = DFFEAS(INCREM(0) $ ((COUNT(0))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \COUNT[0]~15\ = CARRY((INCREM(0) & (COUNT(0))))
-- \COUNT[0]~15COUT1_17\ = CARRY((INCREM(0) & (COUNT(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => INCREM(0),
	datab => COUNT(0),
	aclr => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => COUNT(0),
	cout0 => \COUNT[0]~15\,
	cout1 => \COUNT[0]~15COUT1_17\);

-- Location: LC_X9_Y4_N1
\COUNT[1]\ : maxii_lcell
-- Equation(s):
-- COUNT(1) = DFFEAS(INCREM(1) $ (COUNT(1) $ ((\COUNT[0]~15\))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \COUNT[1]~13\ = CARRY((INCREM(1) & (!COUNT(1) & !\COUNT[0]~15\)) # (!INCREM(1) & ((!\COUNT[0]~15\) # (!COUNT(1)))))
-- \COUNT[1]~13COUT1_18\ = CARRY((INCREM(1) & (!COUNT(1) & !\COUNT[0]~15COUT1_17\)) # (!INCREM(1) & ((!\COUNT[0]~15COUT1_17\) # (!COUNT(1)))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => INCREM(1),
	datab => COUNT(1),
	aclr => GND,
	cin0 => \COUNT[0]~15\,
	cin1 => \COUNT[0]~15COUT1_17\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => COUNT(1),
	cout0 => \COUNT[1]~13\,
	cout1 => \COUNT[1]~13COUT1_18\);

-- Location: LC_X9_Y4_N2
\COUNT[2]\ : maxii_lcell
-- Equation(s):
-- COUNT(2) = DFFEAS(INCREM(2) $ (COUNT(2) $ ((!\COUNT[1]~13\))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \COUNT[2]~11\ = CARRY((INCREM(2) & ((COUNT(2)) # (!\COUNT[1]~13\))) # (!INCREM(2) & (COUNT(2) & !\COUNT[1]~13\)))
-- \COUNT[2]~11COUT1_19\ = CARRY((INCREM(2) & ((COUNT(2)) # (!\COUNT[1]~13COUT1_18\))) # (!INCREM(2) & (COUNT(2) & !\COUNT[1]~13COUT1_18\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => INCREM(2),
	datab => COUNT(2),
	aclr => GND,
	cin0 => \COUNT[1]~13\,
	cin1 => \COUNT[1]~13COUT1_18\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => COUNT(2),
	cout0 => \COUNT[2]~11\,
	cout1 => \COUNT[2]~11COUT1_19\);

-- Location: LC_X9_Y4_N3
\COUNT[3]\ : maxii_lcell
-- Equation(s):
-- COUNT(3) = DFFEAS(COUNT(3) $ (INCREM(3) $ ((\COUNT[2]~11\))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \COUNT[3]~9\ = CARRY((COUNT(3) & (!INCREM(3) & !\COUNT[2]~11\)) # (!COUNT(3) & ((!\COUNT[2]~11\) # (!INCREM(3)))))
-- \COUNT[3]~9COUT1_20\ = CARRY((COUNT(3) & (!INCREM(3) & !\COUNT[2]~11COUT1_19\)) # (!COUNT(3) & ((!\COUNT[2]~11COUT1_19\) # (!INCREM(3)))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => COUNT(3),
	datab => INCREM(3),
	aclr => GND,
	cin0 => \COUNT[2]~11\,
	cin1 => \COUNT[2]~11COUT1_19\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => COUNT(3),
	cout0 => \COUNT[3]~9\,
	cout1 => \COUNT[3]~9COUT1_20\);

-- Location: LC_X9_Y4_N4
\COUNT[4]\ : maxii_lcell
-- Equation(s):
-- COUNT(4) = DFFEAS(COUNT(4) $ (INCREM(4) $ ((!\COUNT[3]~9\))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \COUNT[4]~7\ = CARRY((COUNT(4) & ((INCREM(4)) # (!\COUNT[3]~9COUT1_20\))) # (!COUNT(4) & (INCREM(4) & !\COUNT[3]~9COUT1_20\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => COUNT(4),
	datab => INCREM(4),
	aclr => GND,
	cin0 => \COUNT[3]~9\,
	cin1 => \COUNT[3]~9COUT1_20\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => COUNT(4),
	cout => \COUNT[4]~7\);

-- Location: LC_X10_Y3_N5
\process_0~3\ : maxii_lcell
-- Equation(s):
-- \process_0~3_combout\ = (\process_0~0_combout\ & (!\DATA~combout\(5) & (\DATA~combout\(6) & !\DATA~combout\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~0_combout\,
	datab => \DATA~combout\(5),
	datac => \DATA~combout\(6),
	datad => \DATA~combout\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~3_combout\);

-- Location: LC_X10_Y3_N6
\INCREM[6]\ : maxii_lcell
-- Equation(s):
-- INCREM(6) = ((GLOBAL(\process_0~2_combout\) & (\process_0~3_combout\)) # (!GLOBAL(\process_0~2_combout\) & ((INCREM(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "afa0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~3_combout\,
	datac => \process_0~2_combout\,
	datad => INCREM(6),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => INCREM(6));

-- Location: LC_X10_Y3_N9
\process_0~4\ : maxii_lcell
-- Equation(s):
-- \process_0~4_combout\ = (\process_0~0_combout\ & (\DATA~combout\(5) & (!\DATA~combout\(4))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0808",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \process_0~0_combout\,
	datab => \DATA~combout\(5),
	datac => \DATA~combout\(4),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \process_0~4_combout\);

-- Location: LC_X10_Y3_N1
\INCREM[5]\ : maxii_lcell
-- Equation(s):
-- INCREM(5) = ((GLOBAL(\process_0~2_combout\) & (\process_0~4_combout\)) # (!GLOBAL(\process_0~2_combout\) & ((INCREM(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "cfc0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \process_0~4_combout\,
	datac => \process_0~2_combout\,
	datad => INCREM(5),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => INCREM(5));

-- Location: LC_X9_Y4_N5
\COUNT[5]\ : maxii_lcell
-- Equation(s):
-- COUNT(5) = DFFEAS(INCREM(5) $ (COUNT(5) $ ((\COUNT[4]~7\))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \COUNT[5]~5\ = CARRY((INCREM(5) & (!COUNT(5) & !\COUNT[4]~7\)) # (!INCREM(5) & ((!\COUNT[4]~7\) # (!COUNT(5)))))
-- \COUNT[5]~5COUT1_21\ = CARRY((INCREM(5) & (!COUNT(5) & !\COUNT[4]~7\)) # (!INCREM(5) & ((!\COUNT[4]~7\) # (!COUNT(5)))))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => INCREM(5),
	datab => COUNT(5),
	aclr => GND,
	cin => \COUNT[4]~7\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => COUNT(5),
	cout0 => \COUNT[5]~5\,
	cout1 => \COUNT[5]~5COUT1_21\);

-- Location: LC_X9_Y4_N6
\COUNT[6]\ : maxii_lcell
-- Equation(s):
-- COUNT(6) = DFFEAS(COUNT(6) $ (INCREM(6) $ ((!(!\COUNT[4]~7\ & \COUNT[5]~5\) # (\COUNT[4]~7\ & \COUNT[5]~5COUT1_21\)))), GLOBAL(\CLK~combout\), VCC, , , , , , )
-- \COUNT[6]~3\ = CARRY((COUNT(6) & ((INCREM(6)) # (!\COUNT[5]~5\))) # (!COUNT(6) & (INCREM(6) & !\COUNT[5]~5\)))
-- \COUNT[6]~3COUT1_22\ = CARRY((COUNT(6) & ((INCREM(6)) # (!\COUNT[5]~5COUT1_21\))) # (!COUNT(6) & (INCREM(6) & !\COUNT[5]~5COUT1_21\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "698e",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	dataa => COUNT(6),
	datab => INCREM(6),
	aclr => GND,
	cin => \COUNT[4]~7\,
	cin0 => \COUNT[5]~5\,
	cin1 => \COUNT[5]~5COUT1_21\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => COUNT(6),
	cout0 => \COUNT[6]~3\,
	cout1 => \COUNT[6]~3COUT1_22\);

-- Location: LC_X9_Y4_N7
\COUNT[7]\ : maxii_lcell
-- Equation(s):
-- COUNT(7) = DFFEAS((COUNT(7) $ ((!\COUNT[4]~7\ & \COUNT[6]~3\) # (\COUNT[4]~7\ & \COUNT[6]~3COUT1_22\) $ (INCREM(7)))), GLOBAL(\CLK~combout\), VCC, , , , , , )

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "c33c",
	operation_mode => "normal",
	output_mode => "reg_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	clk => \CLK~combout\,
	datab => COUNT(7),
	datad => INCREM(7),
	aclr => GND,
	cin => \COUNT[4]~7\,
	cin0 => \COUNT[6]~3\,
	cin1 => \COUNT[6]~3COUT1_22\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => COUNT(7));

-- Location: LC_X10_Y4_N0
\LessThan0~37\ : maxii_lcell
-- Equation(s):
-- \LessThan0~37_cout0\ = CARRY((!COUNT(0) & (\DATA~combout\(0))))
-- \LessThan0~37COUT1_41\ = CARRY((!COUNT(0) & (\DATA~combout\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ff44",
	operation_mode => "arithmetic",
	output_mode => "none",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => COUNT(0),
	datab => \DATA~combout\(0),
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan0~35\,
	cout0 => \LessThan0~37_cout0\,
	cout1 => \LessThan0~37COUT1_41\);

-- Location: LC_X10_Y4_N1
\LessThan0~32\ : maxii_lcell
-- Equation(s):
-- \LessThan0~32_cout0\ = CARRY((COUNT(1) & ((!\LessThan0~37_cout0\) # (!\DATA~combout\(1)))) # (!COUNT(1) & (!\DATA~combout\(1) & !\LessThan0~37_cout0\)))
-- \LessThan0~32COUT1_42\ = CARRY((COUNT(1) & ((!\LessThan0~37COUT1_41\) # (!\DATA~combout\(1)))) # (!COUNT(1) & (!\DATA~combout\(1) & !\LessThan0~37COUT1_41\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "ff2b",
	operation_mode => "arithmetic",
	output_mode => "none",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => COUNT(1),
	datab => \DATA~combout\(1),
	cin0 => \LessThan0~37_cout0\,
	cin1 => \LessThan0~37COUT1_41\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan0~30\,
	cout0 => \LessThan0~32_cout0\,
	cout1 => \LessThan0~32COUT1_42\);

-- Location: LC_X10_Y4_N2
\LessThan0~27\ : maxii_lcell
-- Equation(s):
-- \LessThan0~27_cout0\ = CARRY((\DATA~combout\(2) & ((!\LessThan0~32_cout0\) # (!COUNT(2)))) # (!\DATA~combout\(2) & (!COUNT(2) & !\LessThan0~32_cout0\)))
-- \LessThan0~27COUT1_43\ = CARRY((\DATA~combout\(2) & ((!\LessThan0~32COUT1_42\) # (!COUNT(2)))) # (!\DATA~combout\(2) & (!COUNT(2) & !\LessThan0~32COUT1_42\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "ff2b",
	operation_mode => "arithmetic",
	output_mode => "none",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(2),
	datab => COUNT(2),
	cin0 => \LessThan0~32_cout0\,
	cin1 => \LessThan0~32COUT1_42\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan0~25\,
	cout0 => \LessThan0~27_cout0\,
	cout1 => \LessThan0~27COUT1_43\);

-- Location: LC_X10_Y4_N3
\LessThan0~22\ : maxii_lcell
-- Equation(s):
-- \LessThan0~22_cout0\ = CARRY((COUNT(3) & ((!\LessThan0~27_cout0\) # (!\DATA~combout\(3)))) # (!COUNT(3) & (!\DATA~combout\(3) & !\LessThan0~27_cout0\)))
-- \LessThan0~22COUT1_44\ = CARRY((COUNT(3) & ((!\LessThan0~27COUT1_43\) # (!\DATA~combout\(3)))) # (!COUNT(3) & (!\DATA~combout\(3) & !\LessThan0~27COUT1_43\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "ff2b",
	operation_mode => "arithmetic",
	output_mode => "none",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => COUNT(3),
	datab => \DATA~combout\(3),
	cin0 => \LessThan0~27_cout0\,
	cin1 => \LessThan0~27COUT1_43\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan0~20\,
	cout0 => \LessThan0~22_cout0\,
	cout1 => \LessThan0~22COUT1_44\);

-- Location: LC_X10_Y4_N4
\LessThan0~17\ : maxii_lcell
-- Equation(s):
-- \LessThan0~17_cout\ = CARRY((\DATA~combout\(4) & ((!\LessThan0~22COUT1_44\) # (!COUNT(4)))) # (!\DATA~combout\(4) & (!COUNT(4) & !\LessThan0~22COUT1_44\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	lut_mask => "ff2b",
	operation_mode => "arithmetic",
	output_mode => "none",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(4),
	datab => COUNT(4),
	cin0 => \LessThan0~22_cout0\,
	cin1 => \LessThan0~22COUT1_44\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan0~15\,
	cout => \LessThan0~17_cout\);

-- Location: LC_X10_Y4_N5
\LessThan0~12\ : maxii_lcell
-- Equation(s):
-- \LessThan0~12_cout0\ = CARRY((COUNT(5) & ((!\LessThan0~17_cout\) # (!\DATA~combout\(5)))) # (!COUNT(5) & (!\DATA~combout\(5) & !\LessThan0~17_cout\)))
-- \LessThan0~12COUT1_45\ = CARRY((COUNT(5) & ((!\LessThan0~17_cout\) # (!\DATA~combout\(5)))) # (!COUNT(5) & (!\DATA~combout\(5) & !\LessThan0~17_cout\)))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "ff2b",
	operation_mode => "arithmetic",
	output_mode => "none",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => COUNT(5),
	datab => \DATA~combout\(5),
	cin => \LessThan0~17_cout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan0~10\,
	cout0 => \LessThan0~12_cout0\,
	cout1 => \LessThan0~12COUT1_45\);

-- Location: LC_X10_Y4_N6
\LessThan0~7\ : maxii_lcell
-- Equation(s):
-- \LessThan0~7_cout0\ = CARRY((\DATA~combout\(6) & ((!\LessThan0~12_cout0\) # (!COUNT(6)))) # (!\DATA~combout\(6) & (!COUNT(6) & !\LessThan0~12_cout0\)))
-- \LessThan0~7COUT1_46\ = CARRY((\DATA~combout\(6) & ((!\LessThan0~12COUT1_45\) # (!COUNT(6)))) # (!\DATA~combout\(6) & (!COUNT(6) & !\LessThan0~12COUT1_45\)))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "ff2b",
	operation_mode => "arithmetic",
	output_mode => "none",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	dataa => \DATA~combout\(6),
	datab => COUNT(6),
	cin => \LessThan0~17_cout\,
	cin0 => \LessThan0~12_cout0\,
	cin1 => \LessThan0~12COUT1_45\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan0~5\,
	cout0 => \LessThan0~7_cout0\,
	cout1 => \LessThan0~7COUT1_46\);

-- Location: LC_X10_Y4_N7
\LessThan0~0\ : maxii_lcell
-- Equation(s):
-- \LessThan0~0_combout\ = ((COUNT(7) & ((!\LessThan0~17_cout\ & \LessThan0~7_cout0\) # (\LessThan0~17_cout\ & \LessThan0~7COUT1_46\) & \DATA~combout\(7))) # (!COUNT(7) & (((!\LessThan0~17_cout\ & \LessThan0~7_cout0\) # (\LessThan0~17_cout\ & 
-- \LessThan0~7COUT1_46\)) # (\DATA~combout\(7)))))

-- pragma translate_off
GENERIC MAP (
	cin0_used => "true",
	cin1_used => "true",
	cin_used => "true",
	lut_mask => "f330",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "cin",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => COUNT(7),
	datad => \DATA~combout\(7),
	cin => \LessThan0~17_cout\,
	cin0 => \LessThan0~7_cout0\,
	cin1 => \LessThan0~7COUT1_46\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan0~0_combout\);

-- Location: LC_X10_Y4_N8
\PWM~0\ : maxii_lcell
-- Equation(s):
-- \PWM~0_combout\ = ((\LessThan0~0_combout\) # ((\Equal0~1_combout\ & \Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ffc0",
	operation_mode => "normal",
	output_mode => "comb_only",
	register_cascade_mode => "off",
	sum_lutc_input => "datac",
	synch_mode => "off")
-- pragma translate_on
PORT MAP (
	datab => \Equal0~1_combout\,
	datac => \Equal0~0_combout\,
	datad => \LessThan0~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \PWM~0_combout\);

-- Location: PIN_F11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 16mA
\PWM~I\ : maxii_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output")
-- pragma translate_on
PORT MAP (
	datain => \PWM~0_combout\,
	oe => VCC,
	padio => ww_PWM);
END structure;


