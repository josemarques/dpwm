--Digital PWM--

Library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_misc.all;
--use ieee.std_logic_arith.all;
--use ieee.std_logic_signed.all;
entity DPWM is
	generic(N: integer :=8);--Number of bits of the counter
	port(	CLK : IN bit;--Latch signal
			DATA : IN std_logic_vector (N-1 DOWNTO 0);--Input data
			PWM : OUT std_logic--PWM signal
	    );
end DPWM;
--
architecture PWM_CLOCK_VARIABLE_DIVIDER of DPWM is
	signal COUNT: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'0');
	signal INCREM: std_logic_vector (N-1 DOWNTO 0):= (0 => '1', others => '0');--(OTHERS=>'0');----Counter increment, equal to 2 **(the number of less significan zeroes of DATA)
	signal MAX: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'1');
	
	--signal DATAIU : std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'0');--Data in use
	--signal DCHNG: std_logic :='1';

begin
	--DCHNG <= or_reduce(DATA xor DATAIU);

	--process(DCHNG)--When new pwm value is changes calculate new increment value
	process(DATA)
	begin
			for i in 0 to (N-1) loop
            if DATA(i) /= '0' then
					--DATAIU<=DATA;
					INCREM<= (others => '0');--Put one in i increment position
					INCREM(i)<='1';
                exit; -- Salir del bucle cuando se encuentra un bit diferente de 0
            end if;
        end loop;	
		--end if;
	end process;
	
	process(CLK)
	begin
			if(CLK'event and CLK='1')then
				COUNT  <= std_logic_vector(unsigned(COUNT)+unsigned(INCREM));
			end if;
		
	end process;
	
	--PWM out signal
	PWM<=	'1' WHEN COUNT<DATA ELSE 
			'1' WHEN DATA=MAX ELSE '0';


end PWM_CLOCK_VARIABLE_DIVIDER;
--BUFFER_REG_N--

--architecture PWM_CLOCK_DIVIDER of DPWM is
--	signal COUNT: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'0');
--	signal MAX: std_logic_vector (N-1 DOWNTO 0):=(OTHERS=>'1');
--
--begin
--
--	process(CLK)
--	begin
--			if(CLK'event and CLK='1')then
--				COUNT  <= std_logic_vector(unsigned(COUNT)+1);
--			end if;
--		
--	end process;
--	
--	--PWM out signal
--	PWM<=	'1' WHEN COUNT<DATA ELSE 
--			'1' WHEN DATA=MAX ELSE '0';
--
--
--end PWM_CLOCK_DIVIDER;
----BUFFER_REG_N--
